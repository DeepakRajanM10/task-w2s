@extends('welcome')
@section('content')

    <div class="col-md-12">
        <h2>Vote Result</h2>
    </div>

    <div class="col-md-6">
        @foreach($polls as $poll)
            <div class="form-group">
                <label>{{ $poll->question }}</label>
                @foreach($pollAnswers as $pollAnswer)
                    @if($poll->id == $pollAnswer->poll_id)
                    <div class="poll-question">
                        <p>{{ $pollAnswer->answer }} <span>({{ $pollAnswer->vote }} Votes)</span></p>
                        <div class="result-bar" style= "width:<?=@round(($pollAnswer->vote/$totalVotes)*100)?>%">
                            <?=@round(($pollAnswer->vote/$totalVotes)*100)?>%
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        @endforeach
    </div>
@endsection