@extends('welcome')
@section('content')

    <div class="col-md-12">
        <h2>Vote here</h2>
    </div>

    <div class="col-lg-12">
        <form method="POST" action="/answer-vote">
            {{ csrf_field() }}
            @foreach($polls as $poll)
                <div class="form-group">
                    <label>{{ $poll->question }}</label>
                    <div class="radio">
                        <label><input type="radio" name="votes[question_{{ $poll->id }}]" value="{{ $poll->option_1 }}">{{ $poll->option_1 }}</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="votes[question_{{ $poll->id }}]" value="{{ $poll->option_2 }}">{{ $poll->option_2 }}</label>
                    </div>
                </div>
            @endforeach
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Vote</button>
            </div>
        </form>
    </div>
 
@endsection