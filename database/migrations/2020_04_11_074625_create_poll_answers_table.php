<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePollAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poll_answers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('poll_id');
            $table->foreign('poll_id')->references('id')->on('polls');
            $table->string('answer');
            $table->integer('vote');
        });
    
        DB::table('poll_answers')->insert([
            [
                'poll_id' => 1,
                'answer' => 'PHP',
                'vote' => 0
            ],
            [
                'poll_id' => 1,
                'answer' => 'Java',
                'vote' => 0
            ],
            [
                'poll_id' => 2,
                'answer' => 'Angular',
                'vote' => 0
            ],
            [
                'poll_id' => 2,
                'answer' => 'ReactJs',
                'vote' => 0
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poll_answers');
    }
}
