<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls', function (Blueprint $table) {
            $table->id();
            $table->string('question');
            $table->string('option_1');
            $table->string('option_2');
        });

        DB::table('polls')->insert([
            [
                'question' => 'What is your favourite language?',
                'option_1' => 'PHP',
                'option_2' => 'Java'
            ],
            [
                'question' => 'What is your favourite framework?',
                'option_1' => 'Angular',
                'option_2' => 'ReactJs'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polls');
    }
}
