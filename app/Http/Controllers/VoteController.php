<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poll;
use App\PollAnswer;

class VoteController extends Controller
{
    /**
     * Show a vote questions
     */
    public function showVoteQuestions()
    {
       $polls = Poll::all();

       return view('vote', [
           'polls' => $polls
       ]);
    }

    /**
     * Answer a vote
     */
    public function answerVote(Request $request)
    {
        $votes = $request->votes;

        foreach($votes as $vote) {
            $pollAnswer = PollAnswer::where('answer', '=', $vote)->update(['vote' => \DB::raw('vote + 1')]);
        }
   
        return redirect()->to(route('vote_result'));
    }

    /**
     * Show a vote results
     */
    public function showVoteResults()
    {
        $polls = Poll::all();
        $pollAnswers = PollAnswer::all();

        $totalVotes = 0;
        foreach ($pollAnswers as $pollAnswer) {
            $totalVotes += $pollAnswer->vote;
        }

        return view('vote_result', [
            'polls' => $polls,
            'pollAnswers' => $pollAnswers,
            'totalVotes'  => $totalVotes
        ]);
    }
}
