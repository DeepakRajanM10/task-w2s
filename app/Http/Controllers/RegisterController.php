<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{
    /**
     * Show registration form
     */
    public function showForm()
    {
        return view('auth.register');
    }

    /**
     * Register user
     * 
     * @param array $request Request of form submitted
     */
    public function registerUser(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = new User;

        $user->name     = $request->name;
        $user->password = $request->password;
        $user->email    = $request->email;

        $user->save();

        auth()->login($user);

        return redirect()->to('/home');
    }
}
