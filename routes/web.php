<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'GreetController@greet');
Route::get('/register', 'RegisterController@showForm')->name('show_register_form');
Route::post('register', 'RegisterController@registerUser');
Route::get('/login', 'LoginController@showLogin')->name('login');
Route::post('login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

Route::group(['middleware' => ['auth']], function() {
    Route::get('/vote', 'VoteController@showVoteQuestions')->name('vote');
    Route::post('/answer-vote', 'VoteController@answerVote')->name('vote_answer');
    Route::get('/vote-result', 'VoteController@showVoteResults')->name('vote_result');
});
